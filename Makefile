# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/13 14:39:08 by jfazakas          #+#    #+#              #
#    Updated: 2015/11/21 15:08:19 by jfazakas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

SRC = src/main.c\
	  src/print_slist.c\
	  src/choose_list_and_add.c\
	  src/delete_hidden_files.c\
	  src/get_atime.c\
	  src/get_files_and_directories.c\
	  src/get_flags.c\
	  src/get_type.c\
	  src/get_max_name_length.c\
	  src/get_mtime.c\
	  src/get_name.c\
	  src/get_path.c\
	  src/get_rows_number.c\
	  src/get_terminal_width.c\
	  src/is_flag.c\
	  src/list_directory.c\
	  src/list_directory_long.c\
	  src/list_directory_simple.c\
	  src/list_files_in_arguments.c\
	  src/list_files_in_directories.c\
	  src/list_row_simple.c\
	  src/recursive_call.c\
	  src/sort_list.c\
	  src/validate_flags.c\
	  src/calculate_paragraphs_number.c\
	  src/get_paragraph_index.c\
	  src/get_paragraphs_count.c\
	  src/get_info.c\
	  src/get_nlink.c\
	  src/get_max_nlink_length.c\
	  src/list_single_file_long.c\
	  src/print_name.c\
	  src/print_type_and_rights.c\
	  src/get_rights.c\
	  src/print_extended_attributes.c\
	  src/print_nlink.c\
	  src/get_owner.c\
	  src/get_max_owner_length.c\
	  src/print_owner.c\
	  src/get_group.c\
	  src/get_max_group_length.c\
	  src/print_group.c\
	  src/get_size.c\
	  src/get_max_size_length.c\
	  src/print_size.c\
	  src/print_time.c\
	  src/print_total_size.c\
	  src/get_total_size.c\
	  src/list_directory_one.c\
	  src/list_directory_columns.c\
	  src/get_ctime.c\

OBJ = main.o\
	  print_slist.o\
	  choose_list_and_add.o\
	  delete_hidden_files.o\
	  get_atime.o\
	  get_files_and_directories.o\
	  get_flags.o\
	  get_max_name_length.o\
	  get_mtime.o\
	  get_name.o\
	  get_path.o\
	  get_rows_number.o\
	  get_terminal_width.o\
	  get_type.o\
	  is_flag.o\
	  list_directory.o\
	  list_directory_long.o\
	  list_directory_simple.o\
	  list_files_in_arguments.o\
	  list_files_in_directories.o\
	  list_row_simple.o\
	  recursive_call.o\
	  sort_list.o\
	  validate_flags.o\
	  calculate_paragraphs_number.o\
	  get_paragraph_index.o\
	  get_paragraphs_count.o\
	  get_info.o\
	  get_nlink.o\
	  get_max_nlink_length.o\
	  list_single_file_long.o\
	  print_name.o\
	  print_type_and_rights.o\
	  get_rights.o\
	  print_extended_attributes.o\
	  print_nlink.o\
	  get_owner.o\
	  get_max_owner_length.o\
	  print_owner.o\
	  get_max_group_length.o\
	  get_group.o\
	  print_group.o\
	  get_size.o\
	  get_max_size_length.o\
	  print_size.o\
	  print_time.o\
	  get_total_size.o\
	  print_total_size.o\
	  list_directory_one.o\
	  list_directory_columns.o\
	  get_ctime.o\

INC = includes/

FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	make -C libft/ fclean
	make -C libft
	gcc $(FLAGS) -I $(INC) -c $(SRC)
	gcc $(OBJ) -L libft/ -lft -o $(NAME)

clean:
	make -C libft/ clean
	rm -f $(OBJ)

fclean: clean
	make -C libft/ fclean
	rm -f $(NAME)

re: fclean all
