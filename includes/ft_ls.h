/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 16:13:35 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 19:05:26 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <stdio.h>
# include <pwd.h>
# include <grp.h>
# include <errno.h>
# include <dirent.h>
# include <sys/ioctl.h>
# include <time.h>
# include <sys/xattr.h>

# define VALID_FLAGS "Raglrtu1"

typedef struct stat	t_stat;
typedef struct	s_info
{
	int			max_nlink_length;
	int			max_owner_length;
	int			max_group_length;
	int			max_size_length;
	int			total_size;
}				t_info;

void			print_slist(t_slist *list);
int				calculate_paragraphs_number(t_slist *file_list,
						t_slist *directory_list);
void			choose_list_and_add(t_slist **file_list, 
						t_slist **directory_list, char *path);
void			delete_hidden_files(t_slist **list, char *flags);
int				get_atime(char *path);
void			get_files_and_directories(t_slist **file_list,
						t_slist **directory_list, int ac, char **av);
char			*get_flags(int ac, char **av);
int				get_paragraph_index(void);
int				get_paragraphs_count(int index);
int				get_max_name_length(t_slist *file_list, int path_boolean);
int				get_mtime(char *path);
int				get_ctime(char *path);
char			*get_name(char *path);
char			*get_path(char *part_1, char *part_2);
int				get_rows_number(t_slist *list, int column_width);
int				get_terminal_width(void);
char			get_type(char *path);
t_info			*get_info(t_slist *info);
int				get_nlink(char *path);
int				get_max_nlink_length(t_slist *list);
char			*get_rights(char *path);
char			*get_owner(char *path);
int				get_max_owner_length(t_slist *list);
char			*get_group(char *path);
int				get_max_group_length(t_slist *list);
char			*get_size(char *path);
int				get_max_size_length(t_slist *list);
int				get_total_size(t_slist *list);
int				is_flag(char *argument);
void			list_directory(char *path, char *flags,
						int path_boolean);
void			list_directory_long(t_slist **file_list,
						char *flags, int path_boolean);
void			list_directory_simple(t_slist **file_list,
						char *flags, int path_boolean);
void			list_directory_columns(t_slist *file_list, int path_boolean);
void			list_directory_one(t_slist *file_list, int path_boolean);
void			list_files_in_arguments(t_slist **file_list,
						char *flags);
void			list_files_in_directories(t_slist *directory_list,
						char *flags);
void			list_row_simple(t_slist *node, int rows,
						int column_width, int path_boolean);
void			list_single_file_long(char *path,
						char *flags, t_info *info, int path_boolean);
void			print_name(char *path, int path_boolean);
void			print_type_and_rights(char *path);
void			print_extended_attributes(char *path);
void			print_nlink(char *path, t_info *info);
void			print_owner(char *path, t_info *info);
void			print_group(char *path, t_info *info);
void			print_size(char *path, t_info *info);
void			print_time(char *path, char *flags);
void			print_total_size(int total_size);
void			recursive_call(t_slist *file_list,
						char *flags, int path_boolean);
void			sort_list(t_slist *list, char *flags);
void			validate_flags(char *flag, int *double_minus);

#endif
