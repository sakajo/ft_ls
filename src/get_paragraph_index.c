/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_paragraphs_count.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 15:18:59 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:22:07 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_paragraph_index(void)
{
	static int	paragraphs = -1;

	paragraphs++;
	return (paragraphs);
}
