/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_name_length.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 09:02:08 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/18 14:11:47 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_max_name_length(t_slist *list, int path_boolean)
{
	int		max_length;
	int		try;
	t_slist	*node;

	max_length = 0;
	node = list;
	while (node)
	{
		if (path_boolean == 0)
			try = ft_strlen(get_name(node->data));
		else
			try = ft_strlen(node->data);
		if (try > max_length)
			max_length = try;
		node = node->next;
	}
	max_length++;
	while (max_length % 8 != 0)
		max_length++;
	return (max_length);
}
