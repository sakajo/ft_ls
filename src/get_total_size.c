/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_total_size.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 20:19:52 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 20:27:43 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_total_size(t_slist *list)
{
	int		total_size;
	t_stat	stats;
	t_slist	*node;

	total_size = 0;
	node = list;
	while (node)
	{
		if (lstat(node->data, &stats) == -1)
			perror("");
		total_size += stats.st_blocks;
		node = node->next;
	}
	return (total_size);
}
