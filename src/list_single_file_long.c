/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_single_file_long.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 18:30:32 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 16:42:43 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
/*
void	ft_acl_search(char *path)
{
	t_stat		stats;

	lstat(path, &stats);
	ft_putnbr((stats.st_gen));
}
*/
void	list_single_file_long(char *path,
		char *flags, t_info *info, int path_boolean)
{
	print_type_and_rights(path);
	print_extended_attributes(path);
	print_nlink(path, info);
	print_owner(path, info);
	print_group(path, info);
	print_size(path, info);
	print_time(path, flags);
	print_name(path, path_boolean);
	flags[0] += 0;
}
