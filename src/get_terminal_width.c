/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_terminal_width.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 10:03:08 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 11:43:47 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_terminal_width(void)
{
	struct winsize	win;

	ioctl(0, TIOCGWINSZ, &win);
	return (win.ws_col);
}
