/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   choose_list_and_add.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 07:44:46 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 18:48:26 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	choose_list_and_add(t_slist **file_list,
		t_slist **directory_list, char *path)
{
	char	type;
	t_slist *node;
	char	*buffer;

	type = get_type(path);
	if (type == 'e')
	{
		ft_putstr("ls: ");
		perror(path);
		return ;
	}
	if (type == 'l')
	{
		buffer = ft_strnew(1024);
		readlink(path, buffer, 1024);
		if (get_type(buffer) == 'd')
		{
			path = buffer;
			type = get_type(path);
		}
	}
	node = ft_slistnew(path);
	if (type == 'd')
		ft_slistadd(directory_list, node);
	else
		ft_slistadd(file_list, node);
}
