/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_group.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:39:17 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 17:03:10 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_group(char *path)
{
	char			*group;
	t_stat			stats;
	struct group	*gr;

	if (lstat(path, &stats) == -1)
		perror("");
	gr = getgrgid(stats.st_gid);
	if (gr)
		group = ft_strdup(gr->gr_name);
	else
		group = ft_itoa(stats.st_gid);
	return (group);
}
