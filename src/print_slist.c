/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_slist.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:02:26 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 08:03:21 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_slist(t_slist *list)
{
	t_slist	*node;

	node = list;
	while (node)
	{
		ft_putendl(node->data);
		node = node->next;
	}
}
