/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 16:12:52 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/20 15:42:59 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		main(int ac, char **av)
{
	char	*flags;
	t_slist	*file_list;
	t_slist	*directory_list;

	flags = get_flags(ac, av);
	get_files_and_directories(&file_list, &directory_list, ac, av);
	sort_list(file_list, flags);
	sort_list(directory_list, flags);
	get_paragraphs_count(calculate_paragraphs_number(file_list,
			directory_list));
	list_files_in_arguments(&file_list, flags);
	list_files_in_directories(directory_list, flags);
	return (0);
}
