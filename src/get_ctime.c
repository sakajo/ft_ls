/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_ctime.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 15:07:23 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 15:08:44 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_ctime(char *path)
{
	t_stat	stats;

	if (lstat(path, &stats) == -1)
		perror("get_ctime");
	return (stats.st_ctime);
}
