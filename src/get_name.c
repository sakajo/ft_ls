/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_name.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 11:44:00 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 11:44:01 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_name(char *path)
{
	int		index;

	index = ft_strlen(path);
	while (index > 0 && path[index - 1] != '/')
		index--;
	return (ft_strdup(path + index));
}
