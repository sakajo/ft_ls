/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_nlink_length.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 18:14:27 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 18:20:24 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_max_nlink_length(t_slist *list)
{
	int		max_nlink;
	int		max_nlink_size;
	int		try;
	t_slist	*node;

	max_nlink = 0;
	node = list;
	while (node)
	{
		try = get_nlink(node->data);
		if (try > max_nlink)
			max_nlink = try;
		node = node->next;
	}
	max_nlink_size = 0;
	while (max_nlink > 0)
	{
		max_nlink_size++;
		max_nlink /= 10;
	}
	return (max_nlink_size);
}
