/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:54:24 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 18:11:19 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_size(char *path, t_info *info)
{
	char	*size;
	int		length;

	ft_putchar(' ');
	size = get_size(path);
	length = ft_strlen(size);
	while (length < info->max_size_length)
	{
		ft_putchar(' ');
		length++;
	}
	ft_putstr(size);
	ft_putchar(' ');
}
