/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive_call.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 11:58:26 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/18 09:14:16 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	recursive_call(t_slist *file_list,
				char *flags, int path_boolean)
{
	t_slist	*recursive_list;
	t_slist	*node;
	t_slist *new_node;

	get_paragraphs_count(2);
	node = file_list;
	recursive_list = NULL;
	while (node)
	{
		if (get_type(node->data) == 'd' && !ft_strequ(get_name(node->data), ".")
				&& !ft_strequ(get_name(node->data), ".."))
		{
			new_node = ft_slistnew(ft_strdup(node->data));
			ft_slistadd(&recursive_list, new_node);
		}
		node = node->next;
	}
	sort_list(recursive_list, flags);
	list_files_in_directories(recursive_list, flags);
	path_boolean += 0;
}
