/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_info.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 18:04:23 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 19:54:18 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_info	*get_info(t_slist *list)
{
	t_info	*info;

	info = (t_info*)malloc(sizeof(t_info));
	info->max_nlink_length = get_max_nlink_length(list);
	info->max_owner_length = get_max_owner_length(list);
	info->max_group_length = get_max_group_length(list);
	info->max_size_length = get_max_size_length(list);
	return (info);
}
