/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_directory_one.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 14:16:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:18:34 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_directory_one(t_slist *file_list, int path_boolean)
{
	t_slist	*node;

	node = file_list;
	while (node)
	{
		if (path_boolean)
			ft_putendl(node->data);
		else
			ft_putendl(get_name(node->data));
		node = node->next;
	}
}
