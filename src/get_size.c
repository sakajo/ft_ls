/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_size.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:47:14 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 18:13:02 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*construct_major_minor(int d_major, int d_minor)
{
	char	*major_minor;
	char	*temp;
	int		index;

	major_minor = ft_strjoin(" ", ft_strjoin(ft_itoa(d_major), ","));
	index = 0;
	while (index < 4 - ft_numlen(d_minor))
	{
		temp = ft_strjoin(major_minor, " ");
		free(major_minor);
		major_minor = temp;
		index++;
	}
	temp = ft_strjoin(major_minor, ft_itoa(d_minor));
	free(major_minor);
	major_minor = temp;
	return (major_minor);
}

char	*get_major_minor(char *path)
{
	char	*major_minor;
	int		device;
	int		d_major;
	int		d_minor;
	t_stat	stats;

	lstat(path, &stats);
	device = stats.st_rdev;
	d_major = major(device);
	d_minor = minor(device);
	major_minor = construct_major_minor(d_major, d_minor);
	return (major_minor);
}

char	*get_size(char *path)
{
	t_stat	stats;
	char	type;

	type = get_type(path);
	if (type == 'b' || type == 'c')
		return (get_major_minor(path));
	if (lstat(path, &stats) == -1)
		perror("");
	return (ft_itoa(stats.st_size));
}
