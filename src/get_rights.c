/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rights.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 18:44:58 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 19:05:24 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	get_read_rights(char *rights, int index, char *octal)
{
	int		group;

	group = octal[1 + index / 3] - 48;
	if (group >= 4)
		rights[index] = 'r';
}

static void	get_write_rights(char *rights, int index, char *octal)
{
	int		group;

	group = octal[1 + index / 3] - 48;
	if (group / 2 % 2 == 1)
		rights[index] = 'w';
}

static void	execute_conditions(char *rights, int index,
		int execute_boolean, int special_boolean)
{
	if (execute_boolean)
	{
		if (special_boolean)
		{
			if (index > 6)
				rights[index] = 't';
			else
				rights[index] = 's';
		}
		else
			rights[index] = 'x';
	}
	else
	{
		if (special_boolean)
		{
			if (index > 6)
				rights[index] = 'T';
			else
				rights[index] = 'S';
		}
	}
}

static void	get_execute_rights(char *rights, int index, char *octal)
{
	int		group;
	int		special;
	int		execute_boolean;
	int		special_boolean;

	group = octal[1 + index / 3] - 48;
	special = octal[0] - 48;
	execute_boolean = 0;
	special_boolean = 0;
	if (group % 2 == 1)
		execute_boolean = 1;
	if (index / 3 == 0)
		special /= 4;
	if (index / 3 == 1)
		special /= 2;
	if (special % 2 == 1)
		special_boolean = 1;
	execute_conditions(rights, index, execute_boolean, special_boolean);
}

char		*get_rights(char *path)
{
	char	*rights;
	char	*octal;
	t_stat	stats;
	int		index;

	lstat(path, &stats);
	octal = ft_itooctal(stats.st_mode);
	octal += ft_strlen(octal) - 4;
	rights = ft_strnew(9);
	ft_memset(rights, '-', 9);
	index = 0;
	while (index < 9)
	{
		if (index % 3 == 0)
			get_read_rights(rights, index, octal);
		else if (index % 3 == 1)
			get_write_rights(rights, index, octal);
		else
			get_execute_rights(rights, index, octal);
		index++;
	}
	return (rights);
}
