/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_nlink.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 18:57:35 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 19:11:49 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_nlink(char *path, t_info *info)
{
	int		nlink;
	int		length;

	nlink = get_nlink(path);
	length = ft_numlen(nlink);
	ft_putchar(' ');
	while (length < info->max_nlink_length)
	{
		ft_putchar(' ');
		length++;
	}
	ft_putnbr(nlink);
	ft_putchar(' ');
}
