/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_owner.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:15:07 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 16:51:07 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_owner(char *path)
{
	char			*owner;
	t_stat			stats;
	struct passwd	*pw;

	if (lstat(path, &stats) == -1)
		perror("");
	pw = getpwuid(stats.st_uid);
	if (pw)
		owner = ft_strdup(pw->pw_name);
	else
		owner = ft_itoa(stats.st_uid);
	return (owner);
}
