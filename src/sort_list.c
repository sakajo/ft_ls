/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:15:00 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/19 14:31:38 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	compare_nodes_alpha(char *path_1, char *path_2, char *flags)
{
	if (!ft_isanyof('r', flags))
		return (ft_strcmp(path_1, path_2));
	else
		return (ft_strcmp(path_2, path_1));
}

static int	compare_nodes_time(char *path_1, char *path_2, char *flags)
{
	if (ft_isanyof('t', flags))
	{
		if (ft_isanyof('u', flags))
		{
			if (!ft_isanyof('r', flags))
				return (get_atime(path_2) - get_atime(path_1));
			else
				return (get_atime(path_1) - get_atime(path_2));
		}
		else
		{
			if (!ft_isanyof('r', flags))
				return (get_mtime(path_2) - get_mtime(path_1));
			else
				return (get_mtime(path_1) - get_mtime(path_2));
		}
	}
	return (0);
}

void		sort_list(t_slist *list, char *flags)
{
	t_slist	*node_i;
	t_slist	*node_j;

	node_i = list;
	while (node_i)
	{
		node_j = node_i->next;
		while (node_j)
		{
			if (compare_nodes_alpha(node_i->data, node_j->data, flags) > 0)
			{
				ft_slistswap(node_i, node_j);
			}
			if (compare_nodes_time(node_i->data, node_j->data, flags) > 0)
			{
				ft_slistswap(node_i, node_j);
			}
			node_j = node_j->next;
		}
		node_i = node_i->next;
	}
}
