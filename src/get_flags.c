/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_options.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 17:09:51 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/20 15:43:17 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_flags(int ac, char **av)
{
	char	*flags;
	char	*temp;
	int		index;
	int		double_minus;

	flags = ft_strnew(0);
	double_minus = 0;
	index = 1;
	while (index < ac && is_flag(av[index]) && double_minus < 1)
	{
		validate_flags(av[index] + 1, &double_minus);
		temp = ft_strjoin(flags, av[index] + 1);
		free(flags);
		flags = temp;
		index++;
	}
	return (flags);
}
