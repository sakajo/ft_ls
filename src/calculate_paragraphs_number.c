/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_groups_number.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 13:11:38 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 17:51:42 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			calculate_paragraphs_number(t_slist *file_list,
						t_slist *directory_list)
{
	int		count;

	count = 0;
	if (file_list != NULL)
		count = 1;
	count += ft_slistsize(directory_list);
	return (count);
}
