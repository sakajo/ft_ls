/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_name.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 18:34:07 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/18 09:36:29 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_name(char *path, int path_boolean)
{
	char	*buffer;

	if (path_boolean == 1)
		ft_putstr(path);
	else
		ft_putstr(get_name(path));
	if (get_type(path) == 'l')
	{
		buffer = ft_strnew(1024);
		if (readlink(path, buffer, 1024))
		{
			ft_putstr(" -> ");
			ft_putstr(buffer);
		}
	}
	ft_putchar('\n');
}
