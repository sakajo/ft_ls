/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_get_paragraphs.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 16:03:23 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:22:05 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_paragraphs_count(int input)
{
	static int	paragraphs = 0;

	if (input > paragraphs)
		paragraphs = input;
	return (paragraphs);
}
