/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_files_and_directories.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 07:39:05 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/20 15:55:37 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	get_files_and_directories(t_slist **file_list,
				t_slist **directory_list, int ac, char **av)
{
	int		index;
	int		arguments;
	int		double_minus;

	*file_list = NULL;
	*directory_list = NULL;
	arguments = 0;
	double_minus = 0;
	index = 1;
	while (index < ac && is_flag(av[index]) && double_minus < 1)
	{
		if (ft_strequ("--", av[index]))
			double_minus++;
		index++;
	}
	while (index < ac)
	{
		choose_list_and_add(file_list, directory_list, av[index]);
		arguments++;
		index++;
	}
	if (arguments == 0)
		choose_list_and_add(file_list, directory_list, ".");
}
