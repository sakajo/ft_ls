/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_row_simple.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 11:37:59 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/18 09:45:46 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_slist	*jump_list(t_slist *node, int jump)
{
	int		index;
	t_slist	*link;

	if (!node)
		return (NULL);
	link = node;
	index = 0;
	while (index < jump)
	{
		link = link->next;
		if (!link)
			return (NULL);
		index++;
	}
	return (link);
}

static void		put_space(int word_length, int column_width, t_slist *next)
{
	if (!next)
		ft_putchar('\n');
	else
	{
		while (word_length < column_width)
		{
			ft_putchar('\t');
			word_length += 8;
		}
	}
}

void			list_row_simple(t_slist *node, int rows,
				int column_width, int path_boolean)
{
	t_slist	*next;
	char	*name;
	int		word_length;

	while (node)
	{
		next = jump_list(node, rows);
		if (path_boolean == 0)
			name = get_name(node->data);
		else
			name = node->data;
		ft_putstr(name);
		word_length = ft_strlen(name);
		put_space(word_length, column_width, next);
		node = next;
	}
	column_width += 0;
	word_length = 0;
	word_length += 0;
}
