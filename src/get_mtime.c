/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_mtime.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:15:17 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:24:14 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_mtime(char *path)
{
	t_stat	stats;

	if (lstat(path, &stats) == -1)
		perror("get_mtime");
	return (stats.st_mtime);
}
