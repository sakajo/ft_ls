/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_atime.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:15:09 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 08:15:13 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_atime(char *path)
{
	t_stat	stats;

	if (lstat(path, &stats) == -1)
		perror("get_atime");
	return (stats.st_atime);
}
