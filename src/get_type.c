/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_path.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 07:51:25 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 18:40:13 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	get_type(char *path)
{
	t_stat	stats;
	mode_t	mode;
	char	type;

	if (lstat(path, &stats) == -1)
		return ('e');
	mode = stats.st_mode;
	if (S_ISREG(mode) == 1)
		type = '-';
	if (S_ISDIR(mode) == 1)
		type = 'd';
	if (S_ISCHR(mode) == 1)
		type = 'c';
	if (S_ISBLK(mode) == 1)
		type = 'b';
	if (S_ISFIFO(mode) == 1)
		type = 'f';
	if (S_ISLNK(mode) == 1)
		type = 'l';
	if (S_ISSOCK(mode) == 1)
		type = 's';
	return (type);
}
