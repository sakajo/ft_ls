/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_directory.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:28:14 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 18:47:48 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_directory(char *path, char *flags, int path_boolean)
{
	DIR				*dir_ptr;
	struct dirent	*drnt;
	t_slist			*node;
	t_slist			*file_list;

	file_list = NULL;
	dir_ptr = opendir(path);
	if (dir_ptr == NULL)
	{
		perror("ls: no_rights");
		return ;
	}
	while ((drnt = readdir(dir_ptr)) != NULL)
	{
		node = ft_slistnew(get_path(path, drnt->d_name));
		ft_slistadd(&file_list, node);
	}
	closedir(dir_ptr);
	if (ft_isanyof('l', flags) || ft_isanyof('g', flags))
		list_directory_long(&file_list, flags, path_boolean);
	else
		list_directory_simple(&file_list, flags, path_boolean);
	if (ft_isanyof('R', flags))
		recursive_call(file_list, flags, path_boolean);
}
