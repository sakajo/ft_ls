/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_directory_columns.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 14:17:36 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:18:25 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_directory_columns(t_slist *file_list, int path_boolean)
{
	t_slist	*node;
	int		index;
	int		rows;
	int		column_width;

	column_width = get_max_name_length(file_list, path_boolean);
	rows = get_rows_number(file_list, column_width);
	node = file_list;
	index = 0;
	while (index < rows)
	{
		list_row_simple(node, rows, column_width, path_boolean);
		node = node->next;
		index++;
	}
}
