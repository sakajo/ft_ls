/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_files_in_arguments.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 11:19:01 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:29:21 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_files_in_arguments(t_slist **file_list, char *flags)
{
	if (!(*file_list))
		return ;
	if (ft_isanyof('l', flags) || ft_isanyof('g', flags))
		list_directory_long(file_list, flags, 1);
	else
		list_directory_simple(file_list, flags, 1);
	get_paragraph_index();
}
