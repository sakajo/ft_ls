/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_flags.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 17:27:47 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 18:25:11 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	validate_flags(char *flag, int *double_minus)
{
	int		index;

	if (ft_strequ(flag, "-"))
	{
		*double_minus = 1;
		return ;
	}
	index = 0;
	while (flag[index] != '\0')
	{
		if (!ft_isanyof(flag[index], VALID_FLAGS))
		{
			ft_putstr("ls: illegal option -- ");
			ft_putchar(flag[index]);
			ft_putstr("\nusage: ls [-");
			ft_putstr(VALID_FLAGS);
			ft_putendl("] [file ...]");
			exit(0);
		}
		index++;
	}
}
