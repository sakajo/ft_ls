/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_time.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 20:07:26 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 16:20:56 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static char	*get_date(time_t time)
{
	char	*time_string;
	char	*date;

	time_string = ctime(&time);
	date = ft_strsub(time_string, 4, 7);
	return (date);
}

static char	*get_hour(time_t time)
{
	char	*time_string;
	char	*hour;

	time_string = ctime(&time);
	hour = ft_strsub(time_string, 11, 5);
	return (hour);
}

static char	*get_year(time_t time)
{
	char	*date;
	char	*year;

	date = ctime(&time);
	year = ft_strsub(date, 19, 5);
	return (year);
}

void	print_time(char *path, char *flags)
{
	time_t	ma_time;
	time_t	current;

	if (ft_isanyof('u', flags) && ft_isanyof('t', flags))
		ma_time = get_atime(path);
	else
		ma_time = get_mtime(path);
	current = time(NULL);
	if (ft_atoi(get_year(ma_time)) < 1970)
		ft_putstr("Jan  1  10000 ");
	else
	{
		ft_putstr(get_date(ma_time));
		if (ft_strequ(get_year(ma_time), get_year(current)))
			ft_putstr(get_hour(ma_time));
		else
			ft_putstr(get_year(ma_time));
		ft_putchar(' ');
	}
}
