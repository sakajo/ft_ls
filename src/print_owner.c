/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_owner.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:26:23 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 19:36:20 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_owner(char *path, t_info *info)
{
	char	*owner;
	int		length;

	owner = get_owner(path);
	length = ft_strlen(owner);
	ft_putstr(owner);
	while (length < info->max_owner_length)
	{
		ft_putchar(' ');
		length++;
	}
	ft_putchar(' ');
}
