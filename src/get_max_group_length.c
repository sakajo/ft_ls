/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_group_length.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:37:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 19:38:57 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_max_group_length(t_slist *list)
{
	int		max_length;
	int		try;
	t_slist	*node;

	max_length = 0;
	node = list;
	while (node)
	{
		try = ft_strlen(get_group(node->data));
		if (try > max_length)
			max_length = try;
		node = node->next;
	}
	return (max_length);
}
