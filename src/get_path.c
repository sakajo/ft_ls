/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_path.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:35:02 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/19 12:56:31 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_path(char *part_1, char *part_2)
{
	char	*path;
	int		len_1;
	int		len_2;
	int		slash;

	len_1 = ft_strlen(part_1);
	len_2 = ft_strlen(part_2);
	slash = 0;
	if (len_1 > 0 && part_1[len_1 - 1] != '/')
		slash = 1;
	path = ft_strnew(len_1 + len_2 + slash);
	ft_strcpy(path, part_1);
	if (slash == 1)
		path[len_1] = '/';
	ft_strcpy(path + len_1 + slash, part_2);
	return (path);
}
