/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_files_in_directories.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:19:25 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:22:12 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_files_in_directories(t_slist *directory_list, char *flags)
{
	t_slist *node;

	node = directory_list;
	while (node)
	{
		if (get_paragraph_index() > 0)
			ft_putchar('\n');
		if (get_paragraphs_count(0) > 1)
		{
			ft_putstr(node->data);
			ft_putendl(":");
		}
		list_directory(node->data, flags, 0);
		node = node->next;
	}
}
