/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_directory_simple.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:46:17 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 14:18:42 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_directory_simple(t_slist **file_list,
				char *flags, int path_boolean)
{
	delete_hidden_files(file_list, flags);
	sort_list(*file_list, flags);
	if (ft_isanyof('1', flags))
		list_directory_one(*file_list, path_boolean);
	else
		list_directory_columns(*file_list, path_boolean);
}
