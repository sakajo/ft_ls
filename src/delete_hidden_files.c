/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_hidden_files.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 11:44:12 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 11:47:46 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	remove_node(t_slist **file_list, t_slist **node, t_slist **prev)
{
	if (*prev == NULL)
	{
		*file_list = (*file_list)->next;
		free((*node)->data);
		free(*node);
		*node = *file_list;
	}
	else
	{
		(*prev)->next = (*node)->next;
		free((*node)->data);
		free(*node);
		*node = (*prev)->next;
	}
}

void		delete_hidden_files(t_slist **file_list, char *options)
{
	t_slist	*node;
	t_slist	*prev;

	if (ft_isanyof('a', options))
		return ;
	prev = NULL;
	node = *file_list;
	while (node)
	{
		if ((get_name(node->data))[0] == '.')
			remove_node(file_list, &node, &prev);
		else
		{
			prev = node;
			node = node->next;
		}
	}
}
