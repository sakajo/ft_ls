/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_size_length.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:50:01 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/21 17:40:09 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_max_size_length(t_slist *list)
{
	int		max_size;
	int		try;
	t_slist	*node;

	max_size = 0;
	node = list;
	while (node)
	{
		try = ft_strlen(get_size(node->data));
		if (try > max_size)
			max_size = try;
		node = node->next;
	}
	return (max_size);
}
