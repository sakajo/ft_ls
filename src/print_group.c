/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_group.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 19:41:49 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 19:45:44 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_group(char *path, t_info *info)
{
	char	*group;
	int		length;

	ft_putchar(' ');
	group = get_group(path);
	length = ft_strlen(group);
	ft_putstr(group);
	while (length < info->max_group_length)
	{
		ft_putchar(' ');
		length++;
	}
	ft_putchar(' ');
}
