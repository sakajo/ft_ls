/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_directory_long.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 08:47:16 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/20 16:09:31 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	list_directory_long(t_slist **file_list,
				char *flags, int path_boolean)
{
	t_slist	*node;
	t_info	*info;

	delete_hidden_files(file_list, flags);
	sort_list(*file_list, flags);
	info = get_info(*file_list);
	node = *file_list;
	if (!path_boolean)
		print_total_size(get_total_size(*file_list));
	while (node)
	{
		list_single_file_long(node->data, flags, info, path_boolean);
		node = node->next;
	}
}
