/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rows_number.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/17 09:10:13 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/17 11:34:32 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_rows_number(t_slist *list, int column_width)
{
	int		rows;
	int		columns;
	int		files;

	columns = get_terminal_width() / column_width;
	files = ft_slistsize(list);
	rows = files / columns;
	while (columns * rows < files)
		rows++;
	return (rows);
}
